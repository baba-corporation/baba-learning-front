import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase/app';
import 'firebase/storage';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {

  private snapshotChangesSubscription: any;

  constructor(
    public afs: AngularFirestore,
    public afAuth: AngularFireAuth
  ){}

  unsubscribeOnLogOut(){
    //remember to unsubscribe from the snapshotChanges
    this.snapshotChangesSubscription.unsubscribe();
  }

  sendImage(image : File){
    return new Promise((resolve , reject) => {
      this.getBase64(image).then(x=>{
        if(image.type.includes('image')){
          this.uploadImage(x , new Date().getTime() , image.type ).then(res=>{
            console.log("uploaded")
            resolve(res);
          }).catch(err=>{
            reject(err)
          })
        }
      });
    })
  }

  getBase64(file) {
    return new Promise((resolve, reject) => {
      debugger
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        let encoded = reader.result.toString().replace(/^data:(.*,)?/, '');
        if ((encoded.length % 4) > 0) {
          encoded += '='.repeat(4 - (encoded.length % 4));
        }
        resolve(encoded);
      };
      reader.onerror = error => reject(error);
    });
  }

  private uploadImage(image64, id , dataType){
    return new Promise<any>((resolve, reject) => {
      let storageRef = firebase.storage().ref();
      let imageRef = storageRef.child('image').child(id.toString());
        imageRef.putString("data:"+dataType+";base64,"+image64, 'data_url')
        .then(snapshot => {
          snapshot.ref.getDownloadURL()
          .then(res => resolve(res))
        }, err => {
          reject(err);
        })
    })
  }
  
}
