import { Component, OnInit } from '@angular/core';
import { FirebaseService } from '../services/firebase.service';
import { HttpClient } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  public response =  "France";

  constructor(private firebase : FirebaseService,
              private http: HttpClient) { }

  ngOnInit() {
  }

  handleFileInput(f){
    let file : File = f
    this.firebase.sendImage(file[0]).then(url=>{
      debugger
    }).catch(err=>{
      console.error(err)
    });
  }

  sendHttpRequest(url : string){
    this.http.get( "URL BACK" , { headers : {
        //set headers
    }} ).subscribe(d=>{

      //set response

    } , err => {
      console.error(err)
    })
  }

}
